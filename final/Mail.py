#! /usr/bin/env python
# -*- coding utf-8 -*-

"""
This files defines the parser of mail files
"""

from Page import Page
import json

class Mail( Page ):
    """
    This class represents a webpage associated to mail objects
    """
    
    def doHead( self ):
        """
        Defines the title of the webpage
        """    
        self.json = json.load( open( self.root_path + self.path ) )
        self.title = self.json["object"]
        
        
    
    def doMenu( self ):
        """
        We prepare the menu for the action that we can do
        """
        split = self.path.split('/')
        file_name = split[-1]
        self.menu = {  "Delete" : "./" + file_name + "/delete", 
                        "Reply": "./" + file_name + "/reply",
                        "Read":"./read" }
        
        
        
    def strMenu( self ):
        """
        The HTML associated with a menu
        """
        html = "<form class=\"delete\" method=\"post\" action=\"" 
        html += self.menu["Delete"] 
        html += "\">"
        html += "<fieldset><input type=\"submit\" value=\"Delete\" /></fieldset>"
        html += "</form>"
        
        
        html += "<form class=\"reply\" method=\"post\" action=\""
        html += self.menu[ "Reply" ]
        html += "\">"
        html += "<fieldset>"
        html += "<textarea name=\"message\" ></textarea>"
        html += "<input type=\"submit\" value=\"Reply\" /></fieldset>"
        
        html += "<hr>"
        self.html += html
        
        
        
    def doBody( self ):
        """
        Parse the content of the message
        """
        self.sender = self.json["from"]
        self.receiver = self.json[ "to" ]
        self.time = self.json["time"]
        self.message = self.json["message"]
        self.object = self.json[ "object" ]
        
        
    
    def strBody( self ):
        """
        Prints the message as a nice body
        """
        html = ""
        html += "<address><p>"
        html += str( self.sender )
        html += "</p></address>"
        html += "<p>To: " 
        html += str( self.receiver )
        html += "</p>"
        html += "<h1>"
        html += str( self.object )
        html += "</h1>"
        html += "<pre>"
        html += self.message
        html += "</pre>"
        
        self.html += html
        
        
    def doFooter( self ):
        """
        In the footer links we have to include the link to this ressource
        """
        split = self.path.split('/')
        file_name = split[-1]
        
        self.footer = { "next": "./" + file_name + "/next",
                        "previous": "./" + file_name + "/previous" }
        
        
    
