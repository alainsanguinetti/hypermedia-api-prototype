#! /usr/bin/env python

"""
This scripts defines the webpages that presents directories
"""

from Page import Page
import os

class Directory( Page ):

    def doBody( self ):
        
        sep = '/'
    
        self.links = "<menu><ol>"
        self.full_path = self.root_path + self.path
        
        for f_name in os.listdir( self.full_path ):
        
            self.links += "<li>"
            self.links += "<a href=\"./" 
            self.links += f_name
            
            if not os.path.isfile( self.full_path + f_name ):
            
                self.links += sep

            self.links += "\">"
            self.links += f_name
            self.links += "</a></li>"
            
            
        self.links += "</ol></menu>"
        
        
        
    def strBody ( self ):
    
        html = "<h1>"
        html += self.title
        html += "</h1>"    

        html += str( self.links )
        
        self.html += html
        
        
