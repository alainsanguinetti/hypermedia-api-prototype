#! /usr/bin/env python

"""
This scripts describes the page object which is the basic component
of the interface
"""

class Page ( object ):
    """
    The Page object is a high level object that provides
    a header, a body, a footer and a proper HTML document
    when printed using the HTML method
    """
    
    def doFolders( self ):
        """
        Creates a structure to represent the folder in which we are
        """            
        # Split the path into breadcrumbs
        breadcrumbs = self.path.split( '/' )
        breadcrumbs = breadcrumbs[:-1]
        
        if breadcrumbs[0] == '':
            breadcrumbs[0] = "Home"
        
        self.folders = breadcrumbs
    
    
    
    def doHead( self ):
        """ 
        This method provides all the necessary information to create the head of the document
        """
        
        self.title = self.folders[-1]
        
        
        
    def strHead( self ):
        """
        Creates the html string corresponding to the head
        """
    
        self.html += "<head><title>"
        
        self.html += str( self.title )
        
        self.html += "</title><meta name=\"viewport\" content=\"width=device-width\"/></head>"
        
        
        
    def doHeader ( self ):
        """
        Creates the header
        """

        self.header = self.folders
        
        
    def strHeader( self ):
        """
        String corresponding to the header
        """
        
        # For each breadcrumb create a corresponding header link
        html = "<header>"
        for i in range( 0, len(self.header) ) :
            
            html += "<a href=\"" + ( len(self.header) - i - 1 ) * "../" + "\">" + self.header[i] + "</a> >>"
            
        html += "</header><hr>"
        
        self.html += html
        
        
    def doMenu( self ):
        
        return
        
    def strMenu( self ):
    
        return
    
        
    def doBody( self ):
    
        return
        
    def strBody( self ):
    
        return
        
        
    def doFooter( self ):
        """
        Creates the footer structure
        """
        breadcrumbs = self.path.split( '/' )
        print( str( breadcrumbs ) )
        if ( len( breadcrumbs ) == 2 ):
            previous = ""
        else:
            previous = "./previous"
        
        self.footer = { "next": "./next", "previous": previous }
        
        
    def strFooter( self ):
        """
        provides the HTML code for the footer
        """
        html = "<hr><footer><ul>"
        
        html += "<li><a href="
        html += self.footer[ "next" ]
        html += ">Next</a></li>"
        html += "<li><a href="
        html += self.footer[ "previous" ]
        html += ">Previous</a></li>"
        html += "</ul></footer>"
        
        self.html += html
        
    
    def __init__ ( self, root_path, path ):
        """
        This method constructs the Page object using the path and root_path informations
        """
        self.root_path = root_path
        self.path = path
        
        self.doFolders()
        
        self.doHead()
        self.doHeader()
        self.doMenu()
        self.doBody()
        self.doFooter()
        
        return
        
        
    def HTML ( self ):
        """
        This method generates the HTML code corresponding to the Page
        """
        self.html = "<!DOCTYPE html><html>"
        
        self.strHead()
        self.html += "<body>"
        self.strHeader()
        
        self.html += "<article>"

        self.strMenu()
        self.strBody()
        
        self.html += "</article>"

        self.strFooter()
        self.html += "</body>"
        
        
        self.html += "</html>"
        
        return str( self.html )
    
