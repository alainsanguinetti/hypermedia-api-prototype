#!/usr/bin/env python

"""
This script sets up the server for the "Enluminures" project.
The project is to provide a simple HTML/HTTP API for basic desktop computing tasks 

author: Alain Sanguinetti
license: Creative Commons 3.0 Fr - Share Alike

"""

from http.server import BaseHTTPRequestHandler, HTTPServer
import os
from Page import Page
from Directory import Directory
from Mail import Mail
from Next import Next, Previous
from Image import Image
from Text import Text

# Root path to the user directory
root_folder = "/home/alain/Documents/intermediate_project/user"
sep = "/"

class Front ( BaseHTTPRequestHandler ):
    """
    This object is the front-end of the project, it handles an http request 
    and provides the content back.
    
    """
    
    def fullPath( self ):
        """
        Resolves the absolute path of the requested resource
        """    
        self.full_path = root_folder + self.path
        
        
        
    def send_html( self ):
        """
        Send an html response
        """
    
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write( self.html.encode() )
        
        
    def redirect( self, uri ):
        """
        Redirect the user to a given uri
        """
        self.send_response( 303 )
        self.send_header( 'Location', uri )
        self.end_headers()
        
        
    
    def do_DIR( self ):
        """
        Provides a webpage that describes a directory
        """
        self.html = Directory( root_folder, self.path ).HTML()
        self.send_html()
        
        
    
    def do_NEXT( self ):
        """
        Browse the next item in a depth-first search
        """
        # Compute the path to the next page
        uri = Next( self.full_path )
        
        if ( uri.endswith( "out_of_system" ) ):
            uri = self.full_path.split("/next")
            uri = uri[ 0 ]
            uri = uri.split( root_folder )
            uri = uri[ 1 ] # Pb if root folder is empty
            
            # if a directory add the missing /
        print ( "do_NEXT: path=" + str( self.full_path.split("/next")[0] + "/" + uri ) )
        if ( 
            os.path.isdir( self.full_path.split("/next")[0] + "/" + uri ) 
            and not uri.endswith( "/" ) ):
            uri += "/"
                
        self.send_response( 303 )
        self.send_header( 'Location', uri )
        self.end_headers()
        return
        
    
    
    def do_PREVIOUS( self ):
        """
        This method will present the previous item in a depth-first search
        """
        uri = Previous( self.full_path )
        
        self.send_response( 303 )
        self.send_header( 'Location', uri )
        self.end_headers()
        
        
        
    def do_EML( self ):
        """ 
        Handles Mail files 
        """
        self.html = Mail( root_folder, self.path ).HTML()
        self.send_html()
        
        
        
    def do_JPG( self ):
        """
        Handles JPG files
        """
        self.html = Image( root_folder, self.path ).HTML()
        self.send_html()
        
        
    
    def do_TXT( self ):
        """
        Handles TXT files
        """
        self.html = Text ( root_folder, self.path).HTML()
        self.send_html()
        
        
        
    def do_tobin( self ):
        """
        Serves anyfile
        """
        path = self.full_path.split( '.tobin' )
        path = path[ 0 ]
        data = open ( path, 'rb' )
        
        self.send_response(200)
        self.send_header('Content-type', 'image/jpg')
        self.end_headers()
        
        self.wfile.write( data.read() )
        
        
            
    def do_GET ( self ):
        """
        This method handles GET requests
        """
        self.fullPath()
        # If folder
        if os.path.isdir(self.full_path):
            print("this is a directory")
            if (  not self.full_path.endswith( "/" ) ):
                self.redirect( self.path + "/" )
            else:
                self.do_DIR()
        # If next
        elif self.path.endswith("/next"):
            self.do_NEXT()
            return
        elif self.path.endswith("/previous"):
            self.do_PREVIOUS()
            return
        elif self.path.endswith(".eml.json"):
            self.do_EML()
        elif self.path.endswith( ".jpg" ):
            self.do_JPG()
            
        elif self.path.endswith( ".txt" ):
            self.do_TXT()
            
            
        elif self.path.endswith( ".tobin" ):
            self.do_tobin()
        
        # If ...
        
        else:
            if not os.path.exists(self.full_path):
                self.redirect( "http://localhost:8000/" )
            else:
                self.html = Page ( root_folder, self.path ).HTML()
                self.send_html()
            
        return
        
        
    def do_DELETE( self ):
        """
        Handles delete requests
        """
        #resolve file path
        self.fullPath()
        if self.full_path.endswith( "/delete" ):
            self.full_path = self.full_path.split( "/delete" )[0]
            self.path = self.path.split( "/delete" )[0]
                    
        # see if it exists
        if os.path.isfile( self.full_path ):
            # remove 
            #os.remove( self.full_path ):
            print( "EXTEEERMINATE" )
                       
            # redirect to "previous"
            self.redirect( self.path + "/previous" )
            
        else:
            # error 404
            self.send_error( 404 )
            
        return
        
        
        
    def put_email( self, place ):
        """
        Put an email
        """
        # resolve path
        self.fullPath()
        
        import json
        import datetime
        # Prepare json
        # Read original message
        infile = open( self.full_path, 'r' )
        self.json = json.load( infile )
        self.json["from"] = self.json["to"]
        self.json["to"] = self.json["reply-to"]
        self.json[ "message"] = self.rfile.read(int(self.headers['Content-Length']) ).decode('utf-8').split("message=")[-1]
        
        # write the file
        import re
        datetime_s = re.sub('\W+','', str(datetime.datetime.now()) )
        write_path = self.full_path.split( "/mail" )[0]
        write_path += "/mail/outbox" + "/" +datetime_s+ ".eml.json"

        print ( "put_email: write_path=" + write_path )
        mail_f = open( write_path, "w" )
        json.dump( self.json, mail_f )
        
        infile.close()
        mail_f.close()
        
        # Exit
        print ( "put_email: self.path=" + self.path )
        self.redirect( self.path )
        
        
        
        
    def do_PUT( self, filetype, place ):
        """
        Handles the reply to a mail
        """
        
        if filetype == "eml":
            self.put_email(place)
        
        else:
            self.send_error( 400 )
            
        return
        
        
        
        
    def do_POST( self ):
        """
        This will handle all POST requests
        """
        
        if self.path.endswith( "/delete" ):
            self.do_DELETE()
        elif self.path.endswith( "/reply" ):
            self.path = self.path.split( "/reply" )[0]
            self.do_PUT("eml", "outbox")
        else:
            self.send_error( 400 )
            
        return
        print ( self.path )
        post_data = self.rfile.read(int(self.headers['Content-Length']) ).decode('utf-8')
        print( str( post_data ) )
        self.redirect( "http://localhost:8000/" )
            
   
   
def main( ): 
    try:
        server = HTTPServer( ('', 8000), Front )
        print('started httpserver...')
        server.serve_forever()
    except KeyboardInterrupt:
        print('^C received, shutting down server')
        server.socket.close()
    
if __name__ == '__main__':
    main()

