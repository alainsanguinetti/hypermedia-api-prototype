#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
This file provides the capability to go the next item
"""

import os

def open_dir( path ):
    """
    Return the first item in the dir path
    """
    content = os.listdir( path )
    
    #directory not empty
    if len( content ) != 0:    
        if os.path.isdir( path +"/"+  content[0] ):
            return ( "./" + content[0] )
        return ( "./" + content[0] )
        
    # if there is nothing, go to the next item
    else:
        return "../" + next_item( path )
        
        

def next_item( path ):
    """
    This finds the next item for the given path
    """
    print ( "next_item: path=" + str( path ) )
    # List item in the path at level n-1
    item = path.split('/')
    item = item[-1]
    upper = path.split( "/" + item )
    upper = upper[ 0 ]
    
    # For the case of the last page, we don't do anything
    if not upper:
        return "out_of_system"
        

    content = os.listdir( upper )
    index = content.index( item )
    # If our current item is the last
    if index == len( content ) - 1:
        # recursively call on level n-1
        return( "../" + next_item( upper ) )
    # else return the path to the next item
    else:
        return( content[index +1] )
    


def Next( path ):
    """
    This is the frontend to find the next uri according to
    a depth-first search policy
    """
    # if there is "/next" at the end, take it off
    path = path.split('/next')
    path = path[0]
    
    # If we are a directory, open this directory and go to first item
    if os.path.isdir(path):
        uri = open_dir( path )
    
    # if we are not a directory, go to recursively find next item
    else:    
        uri = "../" + next_item( path )
        
    return uri
        
        
        
        
def go_down( path ):
    """
    Go as deep as possible in the exploration 
    """
    print ("go_down: path=" + str( path ) )
    item = path.split('/')
    item = item[-1]
    
    # if we are a file, then nothing to do
    if ( not os.path.isdir( path ) ):
        return item
    
    # if we are a directory, go_down through last item
    else:
        # list contents
        content = os.listdir( path )
        
        #empty directory
        if not content:
            return item + "/"
        else:
            return "/" + go_down( path + content[-1 ] )
    
        
        
        
def open_previous_dir( path ):
    """
    This will return the link to the last item in the previous directory
    """
    # go back one level
    print( "path=" + path )
    item = path.split('/')
    print( path.split( "/" ) )
    item = item[-1]
    print( "item=" + item )
    upper = path.split( "/" + item )
    upper = upper[ 0 ]
    print( "upper=" + upper )
    # list contents
    content = os.listdir( upper )
    
    # open the previous content
    current_index = content.index( item )
    # if we are the first item 
    if current_index == 0:
        # go to upper directory
        return ".."
    # else return the path to the last item
    else:
        return "../" + go_down( upper + "/" + content[current_index -1] )
    
    return "./previous_item"



def Previous( path ):
    """
    This function returns the relative link to the previous document
    """
    # if there is "/previous" at the end, take it off
    path = path.split('/previous')
    path = path[0]
    
    return open_previous_dir( path )
        
