#! /usr/bin/env python
# -*- coding utf-8 -*-

"""
This file defines the text handler
"""

from Page import Page

class Text ( Page ):

    def doHead( self ):
        """
        The title is the name of the file
        """
        item = self.path.split( '/' )
        title = item[ -1 ]
        self.title = title
        
        
        
    def doBody( self ):
        """
        Prepare body
        """
        self.data = open( self.root_path + self.path ).read()
        
        
    def strBody( self ):
        """
        Print the body
        """
        html = ""
        html += "<h1>"
        html += self.title
        html += "</h1>"
        
        html += "<pre>"
        html += str( self.data )
        html += "</pre>"
        
        self.html += html
        
        
        
    def doFooter( self ):
        """
        In the footer links we have to include the link to this ressource
        """
        split = self.path.split('/')
        file_name = split[-1]
        
        self.footer = { "next": "./" + file_name + "/next",
                        "previous": "./" + file_name + "/previous" }
