#! /usr/bin/env python
# -*- coding utf-8 -*-

"""
This file defines the parser of image files
"""

from Page import Page

class Image ( Page ):
    """
    This object represent the image
    """
    
    def doHead( self ):
        """
        The title is the name of the file
        """
        item = self.path.split( '/' )
        title = item[ -1 ]
        self.title = title
        
        
        
    def doBody( self ):
        """
        prepare info for display
        """
        
        self.picture_path = self.path
        
    
    
    def strBody( self ):
        """
        creates the code to embed the image
        """
        html = ""
        html += "<h1>"
        html += self.title
        html += "</h1>"
        html += "<img src=\"../"
        html += self.path
        html += ".tobin\" width=\"200\"/>"
        
        self.html += html
        
        
    def doFooter( self ):
        """
        In the footer links we have to include the link to this ressource
        """
        split = self.path.split('/')
        file_name = split[-1]
        
        self.footer = { "next": "./" + file_name + "/next",
                        "previous": "./" + file_name + "/previous" }
        
        
        
